# RetroBlit Roguelike

This is a simple roguelike game made in RetroBlit following the RoguelikeDev Does The Complete Roguelike Tutorial Series (2019).

https://www.reddit.com/r/roguelikedev/comments/bz6s0j/roguelikedev_does_the_complete_roguelike_tutorial/

When this project is complete it will be included in the set of demos RetroBlit ships with.

## Try it now!

You can play a WebGL game of the game in its current state at:
https://pixeltrollgames.itch.io/rl-tut

## Progress

### Week 1
<img src="/publishing/week1.png" width="640" height="360">
https://www.reddit.com/r/roguelikedev/comments/c1xj5b/roguelikedev_does_the_complete_roguelike_tutorial/

* Part 0 - Setting Up
* Part 1 - Drawing the ‘@’ symbol and moving it around

### Week 2
<img src="/publishing/week2.png" width="640" height="360">
https://www.reddit.com/r/roguelikedev/comments/c52ik4/roguelikedev_does_the_complete_roguelike_tutorial/

* Part 2 - The generic Entity, the render functions, and the map
* Part 3 - Generating a dungeon

### Week 3
<img src="/publishing/week3.png" width="640" height="360">
https://www.reddit.com/r/roguelikedev/comments/c84ryz/roguelikedev_does_the_complete_roguelike_tutorial/

* Part 4 - Field of view
* Part 5 - Placing enemies and kicking them (harmlessly)

### Week 4
<img src="/publishing/week4.png" width="640" height="360">
https://www.reddit.com/r/roguelikedev/comments/caw23f/roguelikedev_does_the_complete_roguelike_tutorial/

* Part 6 - Doing (and taking) some damage
* Part 7 - Creating the Interface

### Week 5
<img src="/publishing/week5.png" width="640" height="360">
https://www.reddit.com/r/roguelikedev/comments/cdt4bf/roguelikedev_does_the_complete_roguelike_tutorial/

* Part 8 - Items and Inventory
* Part 9 - Ranged Scrolls and Targeting

### Week 6
<img src="/publishing/week6.png" width="640" height="360">
https://www.reddit.com/r/roguelikedev/comments/cgo26i/roguelikedev_does_the_complete_roguelike_tutorial/

* Part 10 - Saving and loading
* Part 11 - Delving into the Dungeon

### Week 7
<img src="/publishing/week7.png" width="640" height="360">
https://www.reddit.com/r/roguelikedev/comments/cjmrqo/roguelikedev_does_the_complete_roguelike_tutorial/

* Part 12 - Increasing Difficulty
* Part 13 - Gearing up

### Week X
<img src="/publishing/weekx.png" width="640" height="360">

* Final post-tutorial update with MANY improvements
* Added music and sound effects
* Added more item varieties, some with new gameplay elements (teleport scroll and bow & arrow)
* Added more monster varieties
* Added skills to monsters (eg throw web, slime split)
* Added intro and end levels and a simple story
* Added win condition
* Added a boss (dungeon level 5)
* Added visual effects that don't impede play speed
* Added post processing effects
* Various bug fixes

## Weekly Branches

Progress will happen weekly following the reddit tutorial. This repo has a branch for each week if you want to look back on previous weeks.

## RetroBlit not included
RetroBlit itself is not included in this repo, and the code won't compile without it.

## What is RetroBlit?

RetroBlit is a simple to use game framework implemented as a Unity Asset. It aims to create an ideal, low friction environment
for making pixel-perfect retro games from the early 90s era. RetroBlit takes advantage of the portability, and the ease of
deployment that Unity provides, but does away with the Unity Editor interface in favour of a traditional game loop, and
code-only development. There are no scene graphs, no GameObjects, no MonoBehaviour, there is only a simple low level API for
rendering sprites, fonts, primitives, and tilemaps. RetroBlit also provides a simple API for handling input and playing sound and music.

Check out RetroBlit at: https://assetstore.unity.com/packages/templates/systems/retroblit-102064

[![RetroBlit](/publishing/rb_youtube.png)](https://youtu.be/svEAa9OUZGY "RetroBlit")
